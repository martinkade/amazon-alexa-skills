import { HttpService, Response } from "./service";

export class GluvineService extends HttpService {

    static REQUEST_CODE_XMAS_MARKETS: number = 0x01;

    constructor(private token: string) {
        super();
    }

    public fetchXmasMarkets(locationCode: string, locationName: string, success: Function, error: Function) {
        let url = "http://www.berlin.de/sen/web/service/maerkte-feste/weihnachtsmaerkte/index.php/index.json?page=1";

        this.get(url, (statusCode: number, content: any) => {
            let response = new Response(statusCode, content);
            if (response.isOk()) {
                success(response);
            } else {
                error(response);
            }
        });
    }

    public evaluate(requestCode: number, locationName: string, response: Response): string {
        switch (requestCode) {
            case GluvineService.REQUEST_CODE_XMAS_MARKETS:
                let segmentCount = response.object.index.length;
                return `Ich habe ${response.pluralize(segmentCount, ['Weihnachtsmärkte', 'Weihnachtsmarkt', 'Weihnachtsmärkte'])} in der Nähe von ${locationName} gefunden.`;
            default:
                return "Das kann ich noch nicht.";
        }
    }

}
