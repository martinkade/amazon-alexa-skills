import * as WebRequest from "web-request";

export class HttpService {

    constructor() {

    }

    get<T>(targetUrl: string, callback: Function) {
        (async () => {
            try {
                let response = await WebRequest.json<T>(targetUrl, {
                    method: "GET",
                    throwResponseError: true
                });
                callback(200, response);
            } catch (error) {
                console.log(error);
                callback(0, null);
            }
        })();
    }
    
    generateRandomIndex(from: number, to: number): number {
        let index = function (min: number, max: number) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }(from, to);
        return index;
    }

}

export class Response {

    constructor(public statusCode: number, public object: any) {

    }

    public isOk(): boolean {
        return this.statusCode === 200;
    }

    public pluralize(count: number, strings: string[] = ['Ding', 'Ding', 'Dinge']): string {
        switch (count) {
            case 0:
                return `kein ${strings[0]}`;
            case 1:
                return `ein ${strings[1]}`;
            default:
                return `${count} ${strings[2]}`;
        }
    }

}