"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var service_1 = require("./service");
var GluvineService = (function (_super) {
    __extends(GluvineService, _super);
    function GluvineService(token) {
        var _this = _super.call(this) || this;
        _this.token = token;
        return _this;
    }
    GluvineService.prototype.fetchXmasMarkets = function (locationCode, locationName, success, error) {
        var url = "http://www.berlin.de/sen/web/service/maerkte-feste/weihnachtsmaerkte/index.php/index.json?page=1";
        this.get(url, function (statusCode, content) {
            var response = new service_1.Response(statusCode, content);
            if (response.isOk()) {
                success(response);
            }
            else {
                error(response);
            }
        });
    };
    GluvineService.prototype.evaluate = function (requestCode, locationName, response) {
        switch (requestCode) {
            case GluvineService.REQUEST_CODE_XMAS_MARKETS:
                var segmentCount = response.object.index.length;
                return "Ich habe " + response.pluralize(segmentCount, ['Weihnachtsmärkte', 'Weihnachtsmarkt', 'Weihnachtsmärkte']) + " in der N\u00E4he von " + locationName + " gefunden.";
            default:
                return "Das kann ich noch nicht.";
        }
    };
    return GluvineService;
}(service_1.HttpService));
GluvineService.REQUEST_CODE_XMAS_MARKETS = 0x01;
exports.GluvineService = GluvineService;
//# sourceMappingURL=service-gluvine.js.map