"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Alexa = require("alexa-sdk");
var service_1 = require("./service");
var service_gluvine_1 = require("./service-gluvine");
var service_calendar_1 = require("./service-calendar");
var Handler = (function () {
    function Handler(event, context, callback) {
        var alexa = Alexa.handler(event, context);
        alexa.appId = "amzn1.ask.skill.e132240c-18ff-4dd8-bc5c-ed0634513fb8";
        var todayActionHandlers = Alexa.CreateStateHandler(Handler.STATES.STATE_TODAY, {
            "AMAZON.HelpIntent": function () {
                var self = this;
                var service = new service_1.HttpService();
                var s = [
                    "Ich schreibe gerade noch am Handbuch. Ich denke aber, dass du mittelfristig auch ohne Anleitung auskommst."
                ];
                var index = service.generateRandomIndex(0, s.length - 1);
                var output = s[index];
                self.emit(":ask", output);
            },
            "AMAZON.NoIntent": function () {
                var self = this;
                var service = new service_1.HttpService();
                var s = [
                    "Na gut. Du kannst auch das Türchen eines bestimmten Tages öffnen, indem du sagst: öffne Türchen Nummer ... oder öffne das Türchen am ...?"
                ];
                var index = service.generateRandomIndex(0, s.length - 1);
                var output = s[index];
                self.emit(":ask", output);
            },
            "AMAZON.StopIntent": function () {
                var self = this;
                self.emit("SessionEndedRequest");
            },
            "AMAZON.CancelIntent": function () {
                var self = this;
                self.emit("SessionEndedRequest");
            },
            "AboutIntent": function () {
                var self = this;
                var service = new service_1.HttpService();
                var s = [
                    "Martin hat mir das beigebracht. Falls ich etwas falsch mache, solltest du ihm die Schuld dafür geben."
                ];
                var index = service.generateRandomIndex(0, s.length - 1);
                var output = s[index];
                self.emit(":ask", output);
            },
            "Unhandled": function () {
                var self = this;
                self.emit("Unhandled");
            },
            "GluvineIntent": function () {
                var self = this;
                var request = event.request;
                var locationCode = "48683";
                var locationName = "Ahaus";
                handleGluvineIntent(self, locationCode, locationName);
            },
            "CalendarIntent": function () {
                var self = this;
                var futureDisabled = Handler.FUTURE_DOORS_DISABLED;
                var request = event.request;
                var dateTimeValue = request.intent.slots.DateTime.value;
                var doorNumber = request.intent.slots.DoorNumber.value;
                handleCalendarIntent(self, dateTimeValue, doorNumber, futureDisabled);
            }
        });
        var handlers = {
            "NewSession": function () {
                var self = this;
                var futureDisabled = Handler.FUTURE_DOORS_DISABLED;
                var calendarState = checkCalendarState(new Date(), true, futureDisabled);
                if (calendarState.state === 0 || (calendarState.state === 1 || !futureDisabled)) {
                    self.handler.state = Handler.STATES.STATE_TODAY;
                    self.emit(":ask", calendarState.message);
                }
                else
                    self.emit(":tell", calendarState.message);
            },
            "CalendarIntent": function () {
                var self = this;
                var futureDisabled = Handler.FUTURE_DOORS_DISABLED;
                var request = event.request;
                var dateTimeValue = request.intent.slots.DateTime.value;
                var doorNumber = request.intent.slots.DoorNumber.value;
                var calendarState = checkCalendarState(new Date(), false, futureDisabled);
                if (calendarState.state === 1 || !futureDisabled)
                    handleCalendarIntent(self, dateTimeValue, doorNumber);
                else
                    self.emit(":tell", calendarState.message);
            },
            "Unhandled": function () {
                var self = this;
                var output = "Das kann ich noch nicht.";
                self.emit(":tell", output);
            },
            "SessionEndedRequest": function () {
                var self = this;
                self.attributes['endedSessionCount'] += 1;
                var service = new service_1.HttpService();
                var s = [
                    "Tschüss - bis morgen!"
                ];
                var index = service.generateRandomIndex(0, s.length - 1);
                var output = s[index];
                self.emit(":tell", output);
            }
        };
        /**
         * Check the calendar state.
         *
         * @param date The date, typically today
         * @param session Session flag, default is true
         * @param futureDisabled Disable opening future doors, default is true
         */
        var checkCalendarState = function (date, session, futureDisabled) {
            if (session === void 0) { session = true; }
            if (futureDisabled === void 0) { futureDisabled = true; }
            var state = (function (today) {
                var day = today.getDate();
                var month = today.getMonth() + 1;
                if (month === 11)
                    return -(31 - day);
                else if (month === 12 && day >= 1 && day <= 24)
                    return 1;
                else
                    return 0;
            })(date);
            var service = new service_1.HttpService();
            var s = state === 1 || !futureDisabled
                ? [
                    "Hallo. Soll ich das heutige Türchen für dich öffnen?",
                    "Hallo. Willst du, dass ich das Türchen für heute öffne?",
                    "Hey! Willst du wissen, was sich hinter dem heutigen Türchen versteckt?",
                    "Hey! Soll ich dir verraten, was hinter dem heutigen Türchen steckt?",
                    "Hallo. Darf ich das Türchen für heute öffnen?",
                ] : (state < 0 ? [
                (Math.abs(state) === 1 ? 'Juhu! Morgen' : 'Hallo! In ' + Math.abs(state) + ' Tagen') + " ist es soweit und ich darf das erste T\u00FCrchen f\u00FCr dich \u00F6ffnen. Bitte gedulde dich noch etwas.",
                "Hey! Bald darf ich das erste T\u00FCrchen f\u00FCr dich \u00F6ffnen. " + (Math.abs(state) === 1 ? 'Morgen' : 'In ' + Math.abs(state) + ' Tagen') + " ist es soweit. Bitte hab noch etwas Geduld.",
                "Hey! Es ist schon fast Dezember. " + (Math.abs(state) === 1 ? 'Morgen' : 'In ' + Math.abs(state) + ' Tagen') + " ist es soweit und ich darf das erste T\u00FCrchen f\u00FCr dich \u00F6ffnen. Bitte hab noch etwas Geduld.",
                "Hey! Es ist schon fast Dezember. " + (Math.abs(state) === 1 ? 'Morgen' : 'In ' + Math.abs(state) + ' Tagen') + " darf ich das erste T\u00FCrchen f\u00FCr dich \u00F6ffnen. Bis dahin gedulde dich bitte noch etwas - das Warten lohnt sich, versprochen."
            ] : [
                "Hallo. Du kannst auch das Türchen eines bestimmten Tages öffnen, indem du sagst: öffne Türchen Nummer ... oder öffne das Türchen am ...?"
            ]);
            var index = service.generateRandomIndex(0, s.length - 1);
            var output = s[index];
            return { state: state, message: output };
        };
        /**
         * Handle the calendar intent.
         *
         * @param handler The Amazon Alexa handler
         * @param dateTimeValue The requested input date
         * @param doorNumber The requested door number
         * @param futureDisabled Disable opening future doors, default is true
         */
        var handleCalendarIntent = function (handler, dateTimeValue, doorNumber, futureDisabled) {
            if (futureDisabled === void 0) { futureDisabled = true; }
            var calendarService = new service_calendar_1.CalendarService("");
            var s = [
                "Entschuldige, heute ist nicht mein Tag - ich kann das Türchen nicht finden.",
                "Kannst du bitte prüfen, ob du dich im Datum geirrt hast? Innerhalb der Adventszeit wahr es jedenfalls nicht - vielleicht habe ich dich auch einfach falsch verstanden."
            ];
            var index = calendarService.generateRandomIndex(0, s.length - 1);
            var output = s[index];
            calendarService.fetchFact(dateTimeValue, doorNumber, function (response) {
                try {
                    output = calendarService.evaluate(service_calendar_1.CalendarService.REQUEST_CODE_DAILY_FACT, dateTimeValue, doorNumber, response);
                }
                catch (error) {
                    if (console)
                        console.log(error);
                }
                handler.emit(":tell", output);
            }, function (response) {
                var error = response.object.error;
                handler.emit(":tell", error ? "Ich kann kein T\u00FCrchen finden, weil ein Fehler aufgetreten ist: " + error : output);
            }, futureDisabled);
        };
        /**
         * Handle the gluvine intent.
         *
         * @param handler The Amazon Alexa handler
         * @param locationCode The input location code
         * @param locationName The input location name
         */
        var handleGluvineIntent = function (handler, locationCode, locationName) {
            var gluvineService = new service_gluvine_1.GluvineService("");
            var output = "Im Supermarkt, haha - das kann ich noch nicht.";
            gluvineService.fetchXmasMarkets(locationCode, locationName, function (response) {
                try {
                    output = gluvineService.evaluate(service_gluvine_1.GluvineService.REQUEST_CODE_XMAS_MARKETS, locationName, response);
                }
                catch (error) {
                    if (console)
                        console.log(error);
                }
                handler.emit(":tell", output);
            }, function (response) {
                handler.emit(":tell", output);
            });
        };
        alexa.registerHandlers(handlers, todayActionHandlers);
        alexa.execute();
    }
    return Handler;
}());
Handler.STATES = {
    STATE_TODAY: "_STATE_TODAY"
};
Handler.FUTURE_DOORS_DISABLED = true;
exports.Handler = Handler;
//# sourceMappingURL=index.js.map