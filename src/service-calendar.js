"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var service_1 = require("./service");
var CalendarService = (function (_super) {
    __extends(CalendarService, _super);
    function CalendarService(token) {
        var _this = _super.call(this) || this;
        _this.token = token;
        return _this;
    }
    /**
     * Fetch the fact for a specific date or door number.
     *
     * @param date The date as string or date object
     * @param doorNumber The door number
     * @param success Success callback function
     * @param error Error callback function
     * @param futureDisabled Disable opening future doors, default is true
     */
    CalendarService.prototype.fetchFact = function (date, doorNumber, success, error, futureDisabled) {
        if (futureDisabled === void 0) { futureDisabled = true; }
        // if a specific day is set, extract day (=door number) and month
        if (date) {
            try {
                var _date;
                if (typeof date === 'string' || date instanceof String) {
                    var p = date.match(/(\d+)/g);
                    _date = new Date(parseInt(p[0]), parseInt(p[1]) - 1, parseInt(p[2]));
                }
                else
                    _date = date;
                // day (=door number) [1..28|29|30|31]
                var day = _date.getDate();
                // month [1..12]
                var month = _date.getMonth() + 1;
                success({ object: { fact: this.generateFact(month, day, futureDisabled) } });
            }
            catch (ex) {
                error({ object: { error: null } });
            }
        }
        else if (doorNumber) {
            success({ object: { fact: this.generateFact(12, doorNumber, futureDisabled) } });
        }
        else
            this.fetchFact(new Date(), 0, success, error);
    };
    /**
     * Generate the fact for a specific month and door number.
     *
     * @param month The month
     * @param doorNumber The door number
     * @param futureDisabled Disable opening future doors, default is true
     * @return The fact
     */
    CalendarService.prototype.generateFact = function (month, doorNumber, futureDisabled) {
        if (futureDisabled === void 0) { futureDisabled = true; }
        var today = new Date();
        var day = today.getDate();
        // opening doors in future is not allowed
        if (futureDisabled && (doorNumber > day && month === 12)) {
            var s_1 = [
                "Du bist ganz schon neugierig - ich darf das " + doorNumber + ". T\u00FCrchen noch nicht f\u00FCr dich \u00F6ffnen.",
                "Hab etwas Geduld bitte, das " + doorNumber + ". T\u00FCrchen darf ich leider noch nicht f\u00FCr dich \u00F6ffnen.",
                "Das " + doorNumber + ". T\u00FCrchen darf ich noch nicht \u00F6ffnen. F\u00E4llt mir schwer, aber ich muss dich leider noch etwas zappeln lassen."
            ];
            var index_1 = this.generateRandomIndex(0, s_1.length - 1);
            return s_1[index_1];
        }
        // date is out of advent time span
        if (month !== 12 || doorNumber > 24 || doorNumber < 1) {
            var s_2 = [
                "Du kannst den Kalender nur während der Adventszeit benutzen.",
                "Der Kalender ist nur vom 1. bis zum 24. Dezember funktionsfähig."
            ];
            var index_2 = this.generateRandomIndex(0, s_2.length - 1);
            return s_2[index_2];
        }
        var s = [
            "Das war der Fakt hinter T\u00FCrchen " + doorNumber + ".",
            "Das steckte hinter T\u00FCrchen " + doorNumber + ".",
            "Das war der Fakt hinter dem " + doorNumber + ". T\u00FCrchen.",
            "Das steckte hinter dem " + doorNumber + ". T\u00FCrchen.",
        ];
        var index = this.generateRandomIndex(0, s.length - 1);
        // return `${CalendarService.FACT_LIBRARY[doorNumber]} ${s[index]}`;
        return "" + CalendarService.FACT_LIBRARY[doorNumber];
    };
    CalendarService.prototype.evaluate = function (requestCode, date, doorNumber, response) {
        switch (requestCode) {
            case CalendarService.REQUEST_CODE_DAILY_FACT:
                var fact = response.object.fact;
                return fact;
            default:
                return "Das kann ich noch nicht.";
        }
    };
    return CalendarService;
}(service_1.HttpService));
CalendarService.REQUEST_CODE_DAILY_FACT = 0x01;
CalendarService.FACT_LIBRARY = [
    /*00*/ "Der Advent ist Out-of-bounds",
    /*01*/ "Die Adventszeit soll auf Weihnachten vorbereiten. Wusstest du, dass laut einer Umfrage jeder zehnte Deutsche keine Ahnung hat, warum Weihnachten überhaupt gefeiert wird? Ganz schön traurig - eigentlich. Wie denkst du darüber?",
    /*02*/ "Kinder werden unhöflicher. Die amerikanische Post bekommt jedes Jahr Hunderttausende von Briefen, die an den Weihnachtsmann adressiert sind. Eine Untersuchung ergab, dass Kinder, die einen Brief ausformulierten, generell höflicher waren als solche, die einfach nur eine simple Wunschliste aufschrieben. Grundsätzlich aber, wurden die Kinder im Laufe der Jahre immer unhöflicher. Ein Kind schickte dem Weihnachtsmann sogar eine Morddrohung.",
    /*03*/ "In Großbritannien können Weihnachtsmänner Seminare besuchen, um alles über die trendigsten Spielzeuge oder Videogames zu lernen. Der Kurs, der ebenfalls eine Einführung in die Jugendsprache beinhaltet, wird in der Londoner Oxford Street für jeweils 20 Weihnachtsmänner angeboten.",
    /*04*/ "Der älteste Weihnachtsmarkt in Deutschland ist der sogenannte Wenzelsmarkt in Bautzen. Dieser wurde 1384 das erste mal urkundlich erwähnt. Der größte Weihnachtsmarkt ist hingegen der Dortmunder Weihnachtsmarkt mit jährlich rund 3,7 Millionen Besuchern und etwa 300 Marktständen.",
    /*05*/ "Der Alkoholkonsum der Deutschen steigt unter anderem glühwein- und feuerzangenbowle-bedingt im Dezember um rund 36 Prozent.",
    /*06*/ "Kugelrund, mit Zipfelmütze, rotem Filzanzug, Apfelbäckchen und Rauschebart: so sieht der Nikolaus heutzutage aus. Streng genommen heißt er aber gar nicht mehr Nikolaus - sondern Weihnachtsmann. Auch mit seinem historischen Vorbild, dem Bischof von Myra, hat er nur noch wenig gemein. Ho-ho-ho rufend und dümmlich grinsend führt er stattdessen gerne seinen Schlitten, der von Rentieren gezogen wird. Knecht Ruprecht ist übrigens in Rente gegangen. Und das Christkind? Ist quasi arbeitslos geworden, denn statt ihm bringt auch in Deutschland inzwischen in vielen Familien der Weihnachtsmann die Geschenke.",
    /*07*/ "Ein Ende der Neunziger publizierter Beitrag befasst sich mit dem Nachweis der Nichtexistenz des Weihnachtsmannes. Anhand von statistischen Zahlen wird darin berechnet, dass er zum Verteilen der Geschenke mit 1040 Sachen unterwegs sein müsste - geht man davon aus, dass er 34 Stunden Zeit hat, um alle Geschenke auszuliefern.",
    /*08*/ "Keine Sorge, Kinder vertragen die Wahrheit über den Weihnachtsmann. Irgendwann erfährt jedes Kind, dass es den Weihnachtsmann gar nicht gibt. Wie sich diese Erfahrung auf Kinder auswirkt, wollte ein Forscherteam der Universität von Montréal wissen. 22 Prozent der Kinder seien lediglich enttäuscht gewesen - verraten fühlten sich nur wenige.",
    /*09*/ "Wer je an der Existenz des Weihnachtsmannes gezweifelt hat, dem seien 24 Millionen Fotos entgegengehalten. So oft fotografieren die Deutschen den Weihnachtsmann pro Jahr.",
    /*10*/ "Jeder träumt davon - und doch kommt es fast so selten vor wie ein Sechser im Lotto: weiße Weihnachten. 1981 lag um den 24. Dezember das letzte mal flächendeckend Schnee in Deutschland.",
    /*11*/ "Wusstest du, dass der Weihnachtssong Jingle Bells ursprünglich für Thanksgiving und nicht für Weihnachten geschrieben wurde?",
    /*12*/ "Männliche Rentiere werfen jedes Jahr zur Weihnachtszeit ihr Geweih ab. Demnach müssen die Rentiere des Weihnachtsmannes - da sie alle ein Geweih tragen, entweder weiblich oder kastriert sein.",
    /*13*/ "Norwegische Wissenschaftler gehen davon aus, dass die rot leuchtende Nase von Rudolph dem Rentier auf eine parasitäre Infektion der Atmungsorgane zurückzuführen ist.",
    /*14*/ "Mit Ausnahme von Bayern und Sachsen können Häftlinge in Deutschland im Dezember auf Amnestie hoffen, wenn sie im darauffolgenden Januar ohnehin ihre Strafe abgesessen hätten. Dadurch werden jährlich mehr als 2000 Häftlinge in 14 Bundesländern vorzeitig entlassen.",
    /*15*/ "Der älteste Nussknacker stammt aus dem Jahr 1591 und ist heute im Wiener Völkerkundemuseum zu sehen. Wer ihn erschuf, ist leider nicht bekannt. Doch die Ära der kunstvollen Helfer läutete in jedem Fall der aus dem Erzgebirge stammende Zimmermann Friedrich Wilhelm Füchtner ein. Er kam 1870 auf die Idee, Nussknacker nicht mehr von Hand zu schnitzen, sondern an der Maschine zu drechseln.",
    /*16*/ "Hast du gewusst, dass das Weihnachstfest in England zwischen 1647 und 1660 verboten war? Cromwell, das damalige Staatsoberhaupt, hielt es für unmoralisch an einem der heiligsten Tage des Jahres zu feiern. Wer in ausgelassener Stimmung erwischt wurde, musste sich auf eine hohe Haftstrafen einstellen.",
    /*17*/ "Vier Stunden musste ein 12-Jähriger auf der Polizeiwache einer US-amerikanischen Kleinstadt verbringen, weil er sein Weihnachtsgeschenk vorzeitig ausgepackt und benutzt hatte. Die Mutter hatte den Jungen in Handschellen abführen lassen, um ihm eine Lektion zu erteilen.",
    /*18*/ "Acht Prozent der Deutschen leiden unter Weihnachtsstress. Frauen belastet vor allem die gefühlte Verantwortung für ein harmonisches Fest, Männern raubt die Geschenksuche den Nerv.",
    /*19*/ "Früher war mehr Lametta, sagt Opa Hoppenstedt im Loriot-Sketch. Wie recht er doch hat. Denn die Lametta-Produktion ist in Deutschland in den vergangenen 20 Jahren um fast 70 Prozent zurückgegangen. Das Wort leitet sich übrigens vom italienischen Lama ab und bedeutet Metallblatt. Wer sich schon immer gefragt hat, wieso Lametta überhaupt je am Baum hing: die Glitzerfäden sollen Eiszapfen darstellen.",
    /*20*/ "Weißt du warum die Briten den zweiten Weihnachtstag als Boxing Day bezeichnen. Nicht etwa, weil sie nach der Bescherung so frustriert sind, dass es am 26. Dezember in den Pubs besonders oft zu Massenschlägereien kommt, nein. Der Begriff geht auf eine alte Tradition zurück. Früher überreichten Arbeitgeber ihren Angestellten an diesem Tag das Weihnachtsgeld und ein Geschenk in einer Schachtel, der Box.",
    /*21*/ "Einst wurden in den Christbaum vor allem Äpfel, Nüsse, Gebäck und Bonbons gehängt, weshalb man ihn auch Zuckerbaum nannte. Heute gibt es in vielen Läden und auf Weihnachtsmärkten eine große Auswahl an Christbaumschmuck in unterschiedlichstem Design, darunter auch Kuriositäten wie Weihnachtsgurken. Gurken? Wer, bitte schön, hängt sich Gurken in den Christbaum? Die Deutschen. Das Behaupten zumindest die Amerikaner. Egal, wer`s nun erfunden hat. Klar ist, man versteckt das Gürkchen zwischen den Zweigen - und wer es zuerst entdeckt darf als Erster seine Geschenke auspacken.",
    /*22*/ "Schenken macht glücklich. Eine Untersuchung zeigte, dass je großzügiger jemand war, desto glücklicher war er auch. Ohnehin sollte es beim Schenken aber nicht auf den materiellen Wert ankommen.",
    /*23*/ "Auspacken macht glücklich. Ganz gleich, wie viel Geld sie ausgegeben haben - verpacken Sie die Geschenke unbedingt! Verpackte Geschenke werden einer Studie nach vom Empfänger mehr gemocht als unverpackte. Alleine der Anblick des Geschenkpapiers sorgt demnach bereits für gute Laune.",
    /*24*/ "Essen gehen? Mit Freunden feiern? Oder gar weit weg fahren? Nichts da! Es bleibt dabei: die Deutschen mögen an Weihnachten keine Experimente. Laut einer Umfrage feiern neun von zehn Deutschen das Fest ganz traditionell zu Hause im Kreis ihrer Lieben - und zwar möglichst mit Baum, Braten und Bescherung. Ein guter Hinweis darauf, was Weihnachten wirklich zählen sollte. In diesem Sinne: frohe Weihnachten!"
];
exports.CalendarService = CalendarService;
//# sourceMappingURL=service-calendar.js.map