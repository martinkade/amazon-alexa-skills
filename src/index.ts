import * as Alexa from "alexa-sdk";
import { HttpService, Response } from "./service";
import { GluvineService } from "./service-gluvine";
import { CalendarService } from "./service-calendar";

export class Handler {

    static STATES = {
        STATE_TODAY: "_STATE_TODAY"
    };

    static FUTURE_DOORS_DISABLED: boolean = true;

    constructor(event: Alexa.RequestBody, context: Alexa.Context, callback: Function) {
        let alexa = Alexa.handler(event, context);
        alexa.appId = "amzn1.ask.skill.e132240c-18ff-4dd8-bc5c-ed0634513fb8";

        let todayActionHandlers = Alexa.CreateStateHandler(Handler.STATES.STATE_TODAY, {
            "AMAZON.HelpIntent": function () {
                let self: Alexa.Handler = this;

                let service = new HttpService();
                let s = [
                    "Ich schreibe gerade noch am Handbuch. Ich denke aber, dass du mittelfristig auch ohne Anleitung auskommst."
                ];
                let index = service.generateRandomIndex(0, s.length - 1);
                let output: string = s[index];
                self.emit(":ask", output);
            },
            "AMAZON.NoIntent": function () {
                let self: Alexa.Handler = this;

                let service = new HttpService();
                let s = [
                    "Na gut. Du kannst auch das Türchen eines bestimmten Tages öffnen, indem du sagst: öffne Türchen Nummer ... oder öffne das Türchen am ...?"
                ];
                let index = service.generateRandomIndex(0, s.length - 1);
                let output: string = s[index];
                self.emit(":ask", output);
            },
            "AMAZON.StopIntent": function () {
                let self: Alexa.Handler = this;
                self.emit("SessionEndedRequest");
            },
            "AMAZON.CancelIntent": function () {
                let self: Alexa.Handler = this;
                self.emit("SessionEndedRequest");
            },
            "AboutIntent": function () {
                let self: Alexa.Handler = this;

                let service = new HttpService();
                let s = [
                    "Martin hat mir das beigebracht. Falls ich etwas falsch mache, solltest du ihm die Schuld dafür geben."
                ];
                let index = service.generateRandomIndex(0, s.length - 1);
                let output: string = s[index];
                self.emit(":ask", output);
            },
            "Unhandled": function () {
                let self: Alexa.Handler = this;
                self.emit("Unhandled");
            },
            "GluvineIntent": function () {
                let self: Alexa.Handler = this;
                let request = <Alexa.IntentRequest>event.request;
                let locationCode = "48683"
                let locationName = "Ahaus";

                handleGluvineIntent(self, locationCode, locationName);
            },
            "CalendarIntent": function () {
                let self: Alexa.Handler = this;
                let futureDisabled = Handler.FUTURE_DOORS_DISABLED;
                let request = <Alexa.IntentRequest>event.request;
                let dateTimeValue = request.intent.slots.DateTime.value;
                let doorNumber = request.intent.slots.DoorNumber.value;

                handleCalendarIntent(self, dateTimeValue, doorNumber, futureDisabled);
            }
        });

        let handlers: Alexa.Handlers = {
            "NewSession": function () {
                let self: Alexa.Handler = this;
                let futureDisabled = Handler.FUTURE_DOORS_DISABLED;

                let calendarState = checkCalendarState(new Date(), true, futureDisabled);
                if (calendarState.state === 0 || (calendarState.state === 1 || !futureDisabled)) {
                    self.handler.state = Handler.STATES.STATE_TODAY;
                    self.emit(":ask", calendarState.message);
                } else self.emit(":tell", calendarState.message);
            },
            "CalendarIntent": function () {
                let self: Alexa.Handler = this;
                let futureDisabled = Handler.FUTURE_DOORS_DISABLED;
                let request = <Alexa.IntentRequest>event.request;
                let dateTimeValue = request.intent.slots.DateTime.value;
                let doorNumber = request.intent.slots.DoorNumber.value;

                let calendarState = checkCalendarState(new Date(), false, futureDisabled);
                if (calendarState.state === 1 || !futureDisabled) handleCalendarIntent(self, dateTimeValue, doorNumber);
                else self.emit(":tell", calendarState.message);
            },
            "Unhandled": function () {
                let self: Alexa.Handler = this;

                let output: string = "Das kann ich noch nicht."
                self.emit(":tell", output);
            },
            "SessionEndedRequest": function () {
                let self: Alexa.Handler = this;
                self.attributes['endedSessionCount'] += 1;

                let service = new HttpService();
                let s = [
                    "Tschüss - bis morgen!"
                ];
                let index = service.generateRandomIndex(0, s.length - 1);
                let output: string = s[index];
                self.emit(":tell", output);
            }
        };

        /**
         * Check the calendar state.
         * 
         * @param date The date, typically today
         * @param session Session flag, default is true
         * @param futureDisabled Disable opening future doors, default is true
         */
        let checkCalendarState = function (date: any, session: boolean = true, futureDisabled: boolean = true) {
            let state: number = (function (today: any) {
                let day: number = today.getDate();
                let month: number = today.getMonth() + 1;
                if (month === 11) return -(31 - day);
                else if (month === 12 && day >= 1 && day <= 24) return 1;
                else return 0;
            })(date);

            let service = new HttpService();
            let s = state === 1 || !futureDisabled
                ? [
                    "Hallo. Soll ich das heutige Türchen für dich öffnen?",
                    "Hallo. Willst du, dass ich das Türchen für heute öffne?",
                    "Hey! Willst du wissen, was sich hinter dem heutigen Türchen versteckt?",
                    "Hey! Soll ich dir verraten, was hinter dem heutigen Türchen steckt?",
                    "Hallo. Darf ich das Türchen für heute öffnen?",
                ] : (state < 0 ? [
                    `${Math.abs(state) === 1 ? 'Juhu! Morgen' : 'Hallo! In ' + Math.abs(state) + ' Tagen'} ist es soweit und ich darf das erste Türchen für dich öffnen. Bitte gedulde dich noch etwas.`,
                    `Hey! Bald darf ich das erste Türchen für dich öffnen. ${Math.abs(state) === 1 ? 'Morgen' : 'In ' + Math.abs(state) + ' Tagen'} ist es soweit. Bitte hab noch etwas Geduld.`,
                    `Hey! Es ist schon fast Dezember. ${Math.abs(state) === 1 ? 'Morgen' : 'In ' + Math.abs(state) + ' Tagen'} ist es soweit und ich darf das erste Türchen für dich öffnen. Bitte hab noch etwas Geduld.`,
                    `Hey! Es ist schon fast Dezember. ${Math.abs(state) === 1 ? 'Morgen' : 'In ' + Math.abs(state) + ' Tagen'} darf ich das erste Türchen für dich öffnen. Bis dahin gedulde dich bitte noch etwas - das Warten lohnt sich, versprochen.`
                ] : [
                        "Hallo. Du kannst auch das Türchen eines bestimmten Tages öffnen, indem du sagst: öffne Türchen Nummer ... oder öffne das Türchen am ...?"
                    ]);
            let index = service.generateRandomIndex(0, s.length - 1);
            let output: string = s[index];
            return { state: state, message: output };
        };

        /**
         * Handle the calendar intent.
         * 
         * @param handler The Amazon Alexa handler
         * @param dateTimeValue The requested input date
         * @param doorNumber The requested door number
         * @param futureDisabled Disable opening future doors, default is true
         */
        let handleCalendarIntent = function (handler: Alexa.Handler, dateTimeValue: any, doorNumber: any, futureDisabled: boolean = true) {
            let calendarService = new CalendarService("");
            let s = [
                "Entschuldige, heute ist nicht mein Tag - ich kann das Türchen nicht finden.",
                "Kannst du bitte prüfen, ob du dich im Datum geirrt hast? Innerhalb der Adventszeit wahr es jedenfalls nicht - vielleicht habe ich dich auch einfach falsch verstanden."
            ];
            let index = calendarService.generateRandomIndex(0, s.length - 1);

            let output: string = s[index];
            calendarService.fetchFact(dateTimeValue, doorNumber, (response: Response) => {
                try {
                    output = calendarService.evaluate(CalendarService.REQUEST_CODE_DAILY_FACT, dateTimeValue, doorNumber, response);
                } catch (error) {
                    if (console) console.log(error);
                }
                handler.emit(":tell", output);
            }, (response: Response) => {
                let error = response.object.error;
                handler.emit(":tell", error ? `Ich kann kein Türchen finden, weil ein Fehler aufgetreten ist: ${error}` : output);
            }, futureDisabled);
        };

        /**
         * Handle the gluvine intent.
         * 
         * @param handler The Amazon Alexa handler
         * @param locationCode The input location code
         * @param locationName The input location name
         */
        let handleGluvineIntent = function (handler: Alexa.Handler, locationCode: string, locationName: string) {
            let gluvineService = new GluvineService("");
            let output: string = "Im Supermarkt, haha - das kann ich noch nicht.";
            gluvineService.fetchXmasMarkets(locationCode, locationName, (response: Response) => {
                try {
                    output = gluvineService.evaluate(GluvineService.REQUEST_CODE_XMAS_MARKETS, locationName, response);
                } catch (error) {
                    if (console) console.log(error);
                }
                handler.emit(":tell", output);
            }, (response: Response) => {
                handler.emit(":tell", output);
            });
        };

        alexa.registerHandlers(handlers, todayActionHandlers);
        alexa.execute();
    }

}