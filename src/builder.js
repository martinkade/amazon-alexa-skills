"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Assessment;
(function (Assessment) {
    Assessment[Assessment["YES"] = 0] = "YES";
    Assessment[Assessment["MAYBE"] = 1] = "MAYBE";
    Assessment[Assessment["NO"] = 2] = "NO";
})(Assessment = exports.Assessment || (exports.Assessment = {}));
//# sourceMappingURL=builder.js.map