# About #

This is a simple Amazon Alexa skill written in Typescript. Representing an Advent calendar,
you can ask Alexa to open certain doors. Behind each of the 24 doors there is an exciting
Xmas related fact.

The facts are collected from the internet so we do not assume any warranty for them to be
absolutely correct or up to date.

### License ###

Copyright (C) 2017 Martin Kade

This software comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome
to redistribute it under the conditions of the GNU GPL v3.

### Privacy policy ###

The skill does not collect or store any user specific information. No information
is transmitted to anywhere in the Internet. There's no tracking neither ads.