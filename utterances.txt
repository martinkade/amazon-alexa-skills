AMAZON.HelpIntent hilfe
AMAZON.StopIntent stop
AMAZON.PauseIntent pause
AMAZON.CancelIntent abbrechen

AboutIntent wer schrieb diese app
AboutIntent von wem diese app ist
AboutIntent wer hat dir das beigebracht

GluvineIntent wo gibt es Glühwein
GluvineIntent wo es Glühwein gibt
GluvineIntent wo kann ich Glühwein trinken
GluvineIntent wo ich Glühwein trinken kann

CalendarIntent Ja
CalendarIntent Klar
CalendarIntent Natürlich
CalendarIntent öffne das {DoorNumber} Türchen
CalendarIntent öffne das Türchen Nummer {DoorNumber}
CalendarIntent öffne das Türchen vom {DateTime}
CalendarIntent öffne das Türchen am {DateTime}
CalendarIntent öffne den {DateTime}
CalendarIntent was ist hinter Türchen {DoorNumber}
CalendarIntent was steckt hinter Türchen {DoorNumber}
CalendarIntent was ist hinter dem {DoorNumber} Türchen 
CalendarIntent was steckt hinter dem {DoorNumber} Türchen 
CalendarIntent nach dem heutigen Türchen 
CalendarIntent nach dem nächsten Türchen
CalendarIntent nach dem Türchen heute
CalendarIntent was sich hinter dem heutigen Türchen versteckt
CalendarIntent was sich hinter dem heutigen Türchen verbirgt
CalendarIntent was hinter dem heutigen Türchen ist
CalendarIntent was hinter dem nächsten Türchen ist
CalendarIntent dass er das heutige Türchen öffnen soll
CalendarIntent dass sich das heutige Türchen öffnen soll
CalendarIntent dass er das nächste Türchen öffnen soll
CalendarIntent dass sich das nächste Türchen öffnen soll