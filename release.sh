#!/bin/bash

rm -rf dist
mkdir dist
cp src/* dist
cp -R node_modules dist
cd dist
zip -r ../archive.zip *
cd ..